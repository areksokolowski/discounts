import uuid
import random

with open('range.txt', 'a') as f:
    for i in range(0, 1000):
        id = str(uuid.uuid4())
        dolars = random.randint(1, 1000)
        cents = random.randint(0, 99)
        strCents = str(cents)
        if len(strCents) == 1:
            strCents = "0" + strCents
        line = id + " " + str(dolars) + '.' + strCents + "\n"
        f.write(line)
