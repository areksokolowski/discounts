package org.areso.inpost.discount.price;

import org.areso.inpost.discount.exceptions.RepositoryException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Repository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class PriceRepository {

    private final Map<String, BigDecimal> repo;

    public PriceRepository() {
        try(var r = new BufferedReader(new InputStreamReader(new ClassPathResource("range.txt").getInputStream()))) {
            repo = r.lines().map(l -> l.split(" "))
                    .collect(Collectors.toMap(line -> line[0], line -> new BigDecimal(line[1])));
        } catch (IOException e) {
            throw new RepositoryException("IOException while initialising PriceRepository, see cause for details.", e);
        }
    }

    BigDecimal findById(String uuid) {
        return repo.get(uuid);
    }

}
