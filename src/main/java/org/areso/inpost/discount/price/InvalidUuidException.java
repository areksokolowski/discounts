package org.areso.inpost.discount.price;

import org.areso.inpost.discount.exceptions.DiscountException;

public class InvalidUuidException extends DiscountException {
    public InvalidUuidException(String message) {
        super(message);
    }
}
