package org.areso.inpost.discount.price;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PriceService {

    private final PriceRepository repo;

    public PriceService(PriceRepository repo) {
        this.repo = repo;
    }

    public BigDecimal unitPrice(String uuid) {
        BigDecimal price = repo.findById(uuid);
        if (price == null) throw new InvalidUuidException("Invalid UUID: " + uuid);
        return price;
    }
}
