package org.areso.inpost.discount.metrics;

import io.micrometer.core.instrument.Counter;

public record CounterWrapper(Counter counter) {

    public void increment() {
        if (counter != null) counter.increment();
    }
}
