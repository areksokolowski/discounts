package org.areso.inpost.discount.calculation;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.areso.inpost.discount.metrics.CounterWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/discount/calculation")
public class CalculationController {

    private final CalculationService service;

    private final Logger logger = LoggerFactory.getLogger(CalculationController.class);

    private final CounterWrapper callsCounter;

    public CalculationController(CalculationService service, MeterRegistry meterRegistry) {
        this.service = service;
        callsCounter = new CounterWrapper(meterRegistry != null
            ? Counter.builder("discount.calculations.count").description("Number of discount calculations calls").register(meterRegistry)
            : null);
    }

    @Operation(
        summary = "Calculates the discount for specified product UUID with given parameters.",
        parameters = {
            @Parameter(name = "uuid", required = true, description = "Product identifier."),
            @Parameter(name = "amount", required = true, description = "Number of units in the order. Have to be above 0."),
            @Parameter(name = "discountPolicy", required = true, description = "Name of the discount policy.")
        })
    @ApiResponses({
        @ApiResponse(responseCode = "200", description = "Successfully calculated"),
        @ApiResponse(responseCode = "404", description = "Not found - The product with given UUID was not found", content = {@Content(examples = {})}),
        @ApiResponse(responseCode = "400", description = "Bad request - invalid amount or invalid discount policy", content = {@Content(examples = {})}),
    })
    @GetMapping(path = "{uuid}")
    public CalculationResult getPrice(@PathVariable String uuid, @RequestParam int amount, @RequestParam String discountPolicy) {
        logger.info(String.format("getPrice for %s, amount=%d, policy=%s", uuid, amount, discountPolicy));
        callsCounter.increment();
        var result = service.getDiscountedPrice(uuid, amount, discountPolicy);
        return new CalculationResult(uuid, amount, discountPolicy, result.originalPrice(), result.discountedPrice());
    }
}
