package org.areso.inpost.discount.calculation;

import org.areso.inpost.discount.exceptions.DiscountException;

public class NegativeAmountException extends DiscountException {
    public NegativeAmountException(int amount) {
        super("Invalid amount: " + amount);
    }
}
