package org.areso.inpost.discount.calculation;

import org.areso.inpost.discount.exceptions.DiscountException;

public class UnknownPolicyException extends DiscountException {
    public UnknownPolicyException(String discountPolicyName) {
        super("Unknown calculation policy: " + discountPolicyName);
    }
}
