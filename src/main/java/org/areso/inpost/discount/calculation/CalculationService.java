package org.areso.inpost.discount.calculation;

import org.areso.inpost.discount.policy.PolicyType;
import org.areso.inpost.discount.policy.PolicyService;
import org.areso.inpost.discount.price.InvalidUuidException;
import org.areso.inpost.discount.price.PriceService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;

@Service
public class CalculationService {

    private final PriceService priceService;
    private final PolicyService policyService;

    public CalculationService(PriceService priceService, PolicyService policyService) {
        this.priceService = priceService;
        this.policyService = policyService;
    }

    Discount getDiscountedPrice(String uuid, int amount, String discountPolicyName) {
        if (ObjectUtils.isEmpty(uuid))
            throw new InvalidUuidException("Invalid UUID: " + uuid);
        if (amount <= 0)
            throw new NegativeAmountException(amount);
        PolicyType policy = PolicyType.fromStr(discountPolicyName);
        if (policy == null)
            throw new UnknownPolicyException(discountPolicyName);
        var priceResult = priceService.unitPrice(uuid);
        var originalPrice = priceResult.multiply(BigDecimal.valueOf(amount));
        BigDecimal finalPrice = policyService.getDiscountedPrice(policy, amount, priceResult);
        return new Discount(originalPrice, finalPrice);
    };
}

record Discount(BigDecimal originalPrice, BigDecimal discountedPrice) {}
