package org.areso.inpost.discount.calculation;

import java.math.BigDecimal;

public record CalculationResult(String uuid, int amount, String discountPolicy, BigDecimal originalPrice, BigDecimal finalPrice) {}
