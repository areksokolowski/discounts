package org.areso.inpost.discount.exceptions;

public abstract class DiscountException extends RuntimeException {

    public DiscountException(String message) {
        super(message);
    }

    public DiscountException(String message, Throwable cause) {
        super(message, cause);
    }
}