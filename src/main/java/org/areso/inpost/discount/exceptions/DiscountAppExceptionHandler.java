package org.areso.inpost.discount.exceptions;

import jakarta.servlet.http.HttpServletResponse;
import org.areso.inpost.discount.calculation.NegativeAmountException;
import org.areso.inpost.discount.calculation.UnknownPolicyException;
import org.areso.inpost.discount.policy.ThresholdException;
import org.areso.inpost.discount.price.InvalidUuidException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

@ControllerAdvice
public class DiscountAppExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(InvalidUuidException.class)
    public HttpServletResponse invalidUuid(InvalidUuidException ex, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value(), ex.getMessage());
        return response;
    }

    @ExceptionHandler(NegativeAmountException.class)
    public HttpServletResponse invalidAmount(NegativeAmountException ex, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        return response;
    }

    @ExceptionHandler(UnknownPolicyException.class)
    public HttpServletResponse invalidDiscountPolicy(UnknownPolicyException ex, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        return response;
    }

    @ExceptionHandler(ThresholdException.class)
    public HttpServletResponse invalidThreshold(UnknownPolicyException ex, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        return response;
    }

    @ExceptionHandler(RepositoryException.class)
    public HttpServletResponse repositoryProblem(UnknownPolicyException ex, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
        return response;
    }

}
