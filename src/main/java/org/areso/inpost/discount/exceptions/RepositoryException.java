package org.areso.inpost.discount.exceptions;

public class RepositoryException extends DiscountException {
    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
