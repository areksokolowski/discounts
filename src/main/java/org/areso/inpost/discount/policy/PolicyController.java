package org.areso.inpost.discount.policy;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/discount/policies")
public class PolicyController {

    private final PolicyService service;

    private final Logger logger = LoggerFactory.getLogger(PolicyController.class);

    public PolicyController(PolicyService service) {
        this.service = service;
    }

    @Operation(
            summary = "Gets amount-based discount policy configuration.",
            description = """
                Configuration contains list of threshold - value pair.
                Threshold is a minimum number of units within an order, that enable this discount.
                Value is the absolute discount value, that will be subtracted from the base price.
                """)
    @GetMapping("amount")
    public ThresholdPolicyConfig readAmountPolicy() {
            return service.amountConfig();
    }

    @Operation(
            summary = "Gets percentage-based discount policy configuration.",
            description = """
                Configuration contains list of threshold - value pair.
                Threshold is a minimum number of units within an order, that enable this discount.
                Value is the percentage, that will be applied as a discount to the base price.
                """)
    @GetMapping("percentage")
    public ThresholdPolicyConfig readPercentagePolicy() {
        return service.percentageConfig();
    }

    @Operation(summary = "Updates amount-based discount policy configuration.")
    @ApiResponses({
        @ApiResponse(responseCode = "201", description = "Policy successfully modified"),
        @ApiResponse(responseCode = "400", description = "Bad request - invalid policy configuration, like: negative numbers, lack of thresholds, duplicated threshold", content = {@Content(examples = {})}),
    })
    @PostMapping("amount")
    public void createAmountConfig(@RequestBody List<ThresholdConfig> config) {
        logger.info("AMOUNT config change requested: " + config);
        service.save(new ThresholdPolicyConfig(PolicyType.AMOUNT, config));
    }

    @Operation(summary = "Updates percentage-based discount policy configuration.")
    @ApiResponses({
        @ApiResponse(responseCode = "201", description = "Policy successfully modified"),
        @ApiResponse(responseCode = "400", description = "Bad request - invalid policy configuration, like negative numbers, lack of thresholds, duplicated threshold", content = {@Content(examples = {})}),
    })
    @PostMapping("percentage")
    public void createPercentageConfig(@RequestBody List<ThresholdConfig> config) {
        logger.info("PERCENTAGE config change requested: " + config);
        service.save(new ThresholdPolicyConfig(PolicyType.PERCENTAGE, config));
    }
}
