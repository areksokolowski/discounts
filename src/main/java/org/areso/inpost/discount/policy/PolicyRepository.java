package org.areso.inpost.discount.policy;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.areso.inpost.discount.exceptions.RepositoryException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Repository
public class PolicyRepository {

    private final String repositoryDir;
    private final boolean useStorage;

    private final Map<PolicyType, Lock> fileLocks = new EnumMap<>(PolicyType.class);
    private final Map<PolicyType, ThresholdPolicyConfig> configs = new EnumMap<>(PolicyType.class);

    public PolicyRepository(@Value("${repository.storage}") boolean useStorage, @Value("${repository.dir}") String repositoryDir) {
        this.useStorage = useStorage;
        this.repositoryDir = repositoryDir;
        if (useStorage)
            initStorage();
        Arrays.stream(PolicyType.values()).forEach(this::initPolicy);
    }

    private void initStorage() {
        Path p = Path.of(repositoryDir);
        if (Files.notExists(p)) {
            try {
                Files.createDirectories(p);
            } catch (IOException e) {
                throw new RepositoryException("IOException while initialising ConfigRepository, see cause for details.", e);
            }
        }
    }

    private void initPolicy(PolicyType policy) {
        if (!policy.isConfigurable()) return;
        saveInternal(readDefaultPolicy(policy));
    }

    private ThresholdPolicyConfig readDefaultPolicy(PolicyType policy) {
        var fileName = policy.name().toLowerCase();
        ThresholdPolicyConfig policyConfig;
        ClassPathResource defaultPolicyResource = new ClassPathResource(fileName + ".default");
        try(var is = defaultPolicyResource.getInputStream()) {
            var mapper = new ObjectMapper();
            policyConfig = mapper.readValue(is, ThresholdPolicyConfig.class);
        } catch (IOException e) {
            throw new RepositoryException("IOException while initialising ConfigRepository, see cause for details.", e);
        }
        return policyConfig;
    }

    private void writePolicyToStorage(ThresholdPolicyConfig policyConfig) {
        if (useStorage) {
            var l = getLock(policyConfig.policyType());
            l.lock();
            var fileName = policyConfig.policyType().name().toLowerCase();
            try (var writer = Files.newOutputStream((Path.of(repositoryDir, fileName)))) {
                var mapper = new ObjectMapper();
                mapper.writeValue(writer, policyConfig);
            } catch (IOException e) {
                throw new RepositoryException("IOException while writing policy: " + fileName + ", see cause for details.", e);
            } finally {
                l.unlock();
            }
        }
    }

    public ThresholdPolicyConfig findById(PolicyType policy) {
        return configs.get(policy);
    }

    public void save(ThresholdPolicyConfig policyConfig) {
        saveInternal(policyConfig);
    }

    private void saveInternal(ThresholdPolicyConfig policyConfig) {
        writePolicyToStorage(policyConfig);
        configs.put(policyConfig.policyType(), policyConfig);
    }

    private synchronized Lock getLock(PolicyType policy) {
        return fileLocks.computeIfAbsent(policy, n -> new ReentrantLock(true));
    }
}
