package org.areso.inpost.discount.policy;

import java.math.BigDecimal;

public record ThresholdConfig(int threshold, BigDecimal value) implements Comparable<ThresholdConfig> {
    @Override
    public int compareTo(ThresholdConfig o) {
        return threshold - o.threshold;
    }

    public ThresholdConfig(int threshold, BigDecimal value) {
        if (threshold <= 0)
            throw new ThresholdException("Threshold should be above 0");
        if (value.signum() < 1)
            throw new ThresholdException("Value should be above 0");
        this.threshold = threshold;
        this.value = value;
    }
}
