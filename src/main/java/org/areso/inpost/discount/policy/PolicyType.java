package org.areso.inpost.discount.policy;

public enum PolicyType {
    NO_DISCOUNT(false),
    AMOUNT(true),
    PERCENTAGE(true);

    private final boolean configurable;

    PolicyType(boolean configurable) {
        this.configurable = configurable;
    }

    public boolean isConfigurable() {
        return configurable;
    }

    public static PolicyType fromStr(String policyName) {
        try {
            return valueOf(policyName.toUpperCase());
        } catch (IllegalArgumentException | NullPointerException e) {
            return null;
        }
    }
}
