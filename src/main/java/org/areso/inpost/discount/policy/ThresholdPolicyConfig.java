package org.areso.inpost.discount.policy;

import java.util.ArrayList;
import java.util.List;

public record ThresholdPolicyConfig(PolicyType policyType, List<ThresholdConfig> thresholds) {
    public ThresholdPolicyConfig(PolicyType policyType, List<ThresholdConfig> thresholds) {
        if (thresholds == null || thresholds.isEmpty())
            throw new ThresholdException("Define at least one threshold");
        this.policyType = policyType;
        var temp = new ArrayList<>(thresholds);
        if (temp.stream().map(ThresholdConfig::threshold).distinct().count() < temp.size())
            throw new ThresholdException("Thresholds have to be unique");
        if (policyType == PolicyType.PERCENTAGE && temp.stream().anyMatch(t -> t.value().floatValue() >= 100))
            throw new ThresholdException("Percentage discount cannot exceed 100");

        this.thresholds = temp.stream().sorted().toList();
    }
}
