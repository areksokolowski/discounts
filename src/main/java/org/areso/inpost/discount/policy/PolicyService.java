package org.areso.inpost.discount.policy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
public class PolicyService {

    private final PolicyRepository repo;

    private final Logger logger = LoggerFactory.getLogger(PolicyService.class);

    public PolicyService(PolicyRepository repo) {
        this.repo = repo;
    }

    public void save(ThresholdPolicyConfig discountConfig) {
        repo.save(discountConfig);
        logger.info(String.format("Policy: %s is modified.", discountConfig.policyType()));
    }

    public ThresholdPolicyConfig amountConfig() {
        return repo.findById(PolicyType.AMOUNT);
    }

    public ThresholdPolicyConfig percentageConfig() {
        return repo.findById(PolicyType.PERCENTAGE);
    }

    public BigDecimal getDiscountedPrice(PolicyType policy, int amount, BigDecimal unitPrice) {
        return switch (policy) {
            case NO_DISCOUNT -> unitPrice.multiply(BigDecimal.valueOf(amount));
            case PERCENTAGE -> getPercentageDiscount(amount, unitPrice);
            case AMOUNT -> getAmountDiscount(amount, unitPrice);
        };
    }

    private BigDecimal getAmountDiscount(int amount, BigDecimal unitPrice) {
        logger.debug(String.format("Calculating amount discount for %d units, unitPrice is %s", amount, unitPrice));
        var initialPrice = unitPrice.multiply(BigDecimal.valueOf(amount));
        var discountValue = findThresholdValue(amount, amountConfig().thresholds());
        logger.debug(String.format("Found discount value: %s", discountValue));
        var proposedPrice = initialPrice.subtract(discountValue);
        if (proposedPrice.signum() < 1)
            proposedPrice = new BigDecimal("0.01");
        logger.debug(String.format("Discounted price: %s", proposedPrice));
        return proposedPrice;
    }

    private BigDecimal getPercentageDiscount(int amount, BigDecimal unitPrice) {
        logger.debug(String.format("Calculating percentage discount for %d units, unitPrice is %s", amount, unitPrice));
        var initialPrice = unitPrice.multiply(BigDecimal.valueOf(amount));
        var discountPercentage = findThresholdValue(amount, percentageConfig().thresholds());
        logger.debug(String.format("Found discount percentage: %s", discountPercentage));
        if (discountPercentage.signum() < 1)
            return initialPrice;
        var discountValue = initialPrice.multiply(discountPercentage).divide(BigDecimal.valueOf(100), RoundingMode.DOWN);
        var proposedPrice = initialPrice.subtract(discountValue);
        if (proposedPrice.signum() < 1)
            proposedPrice = new BigDecimal("0.01");
        logger.debug(String.format("Discounted price: %s", proposedPrice));
        return proposedPrice;
    }

    private BigDecimal findThresholdValue(int amount, List<ThresholdConfig> config) {
        BigDecimal result = BigDecimal.ZERO;
        for (var entry: config) {
            logger.trace(String.format("Checking threshold: %d", entry.threshold()));
            if (amount >= entry.threshold()) {
                result = entry.value();
                logger.trace(String.format("Threshold in range, current value: %s", result));
            } else {
                logger.trace("Amount below threshold, interrupting.");
                break;
            }
        }
        return result;
    }
}
