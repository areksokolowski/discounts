package org.areso.inpost.discount.policy;

import org.areso.inpost.discount.exceptions.DiscountException;

public class ThresholdException extends DiscountException {
    public ThresholdException(String message) {
        super(message);
    }
}
