package org.areso.inpost.discount;

import org.areso.inpost.discount.policy.*;
import org.areso.inpost.discount.calculation.*;
import org.areso.inpost.discount.price.InvalidUuidException;
import org.areso.inpost.discount.price.PriceRepository;
import org.areso.inpost.discount.price.PriceService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ControllersApiTest {

    private static final String TEST_UUID = "e654a198-4055-459d-b05a-b9bc11ff4b6c";
    private static final String TEST_UUID_LOW_PRICE = "3204f444-3e56-4566-bb95-f6b3ab794e16";
    private final PolicyService policyService = new PolicyService(new PolicyRepository(false, null));
    private final PolicyController configCtrl = new PolicyController(policyService);
    private final CalculationController discountCtrl = new CalculationController(new CalculationService(new PriceService(new PriceRepository()), policyService), null);

    @Nested
    @DisplayName("Discount calculations")
    class DiscountCalculations {
        @Test
        @DisplayName("No policy calculation")
        public void noDiscount() {
            var result = discountCtrl.getPrice(TEST_UUID, 10, "no_discount");
            checkDiscountResult(result, TEST_UUID, 10, "no_discount", bd("578.80"), bd("578.80"));
        }

        @Test
        @DisplayName("Percentage-based calculation")
        public void percentageBasedDiscount() {
            var result = discountCtrl.getPrice(TEST_UUID, 10, "percentage");
            checkDiscountResult(result, TEST_UUID, 10, "percentage", bd("578.80"), bd("561.44"));
        }

        @Test
        @DisplayName("Amount-based calculation")
        public void amountBasedDiscount() {
            var result = discountCtrl.getPrice(TEST_UUID, 10, "amount");
            checkDiscountResult(result, TEST_UUID, 10, "amount", bd("578.80"), bd("576.80"));
        }

        @Test
        @DisplayName("Check if price after discount doesn't drop below 0")
        public void tooLowPrice() {
            var result = discountCtrl.getPrice(TEST_UUID_LOW_PRICE, 10, "amount");
            checkDiscountResult(result, TEST_UUID_LOW_PRICE, 10, "amount", bd("0.20"), bd("0.01"));
        }

        @Test
        @DisplayName("Amount-based calculation test - no calculation")
        public void amountBasedDiscountNoDiscount() {
            var result = discountCtrl.getPrice(TEST_UUID, 1, "amount");
            checkDiscountResult(result, TEST_UUID, 1, "amount", bd("57.88"), bd("57.88"));
        }

        @Test
        @DisplayName("Invalid UUID")
        public void invalidUuid() {
            assertThrows(
                    InvalidUuidException.class,
                    () -> discountCtrl.getPrice("invalid-uuid", 10, "percentage"));
        }

        @Test
        @DisplayName("Amount less than 1")
        public void negativeAmount() {
            assertThrows(
                    NegativeAmountException.class,
                    () -> discountCtrl.getPrice(TEST_UUID, 0, "percentage"));
        }

        @Test
        @DisplayName("Invalid discount policy")
        public void invalidDiscount() {
            assertThrows(
                    UnknownPolicyException.class,
                    () -> discountCtrl.getPrice(TEST_UUID, 10, "invalid_discount"));
        }
    }

    @Nested
    @DisplayName("Discount policies management")
    class DiscountPolicyConfiguration {
        @Test
        @DisplayName("Read discount configuration")
        public void readConfig() {
            var result = configCtrl.readPercentagePolicy();
            assertThat(result.thresholds()).containsExactlyInAnyOrder(
                    new ThresholdConfig(50, bd("5")),
                    new ThresholdConfig(10, bd("3")));
        }

        @Test
        @DisplayName("Modify discount configuration")
        public void modifyConfig() {
            var t1 = new ThresholdConfig(10, bd("1"));
            var t2 = new ThresholdConfig(20, bd("2"));
            var t3 = new ThresholdConfig(30, bd("3"));
            var original = configCtrl.readPercentagePolicy();
            configCtrl.createPercentageConfig(List.of(t1, t2, t3));
            var result = configCtrl.readPercentagePolicy();
            assertThat(result.thresholds()).containsExactlyInAnyOrder(t1, t2, t3);
            configCtrl.createPercentageConfig(original.thresholds());
            result = configCtrl.readPercentagePolicy();
            assertThat(result.thresholds()).containsExactlyInAnyOrder(
                    new ThresholdConfig(50, bd("5")),
                    new ThresholdConfig(10, bd("3")));
        }
    }

    @Nested
    @DisplayName("Threshold validations check")
    class ThresholdChecks {
        @Test
        @DisplayName("Negative threshold for discount should fail")
        public void checkNegativeDiscountThreshold() {
            assertThrows(
                    ThresholdException.class,
                    () -> configCtrl.createAmountConfig(List.of(new ThresholdConfig(-1, BigDecimal.ONE))));
        }

        @Test
        @DisplayName("Negative value for discount should fail")
        public void checkNegativeDiscountValue() {
            assertThrows(
                    ThresholdException.class,
                    () -> configCtrl.createAmountConfig(List.of(new ThresholdConfig(10, BigDecimal.ZERO))));
        }

        @Test
        @DisplayName("Duplicating threshold in configuration should fail")
        public void duplicatedThresholdShouldFail() {
            assertThrows(
                    ThresholdException.class,
                    () -> configCtrl.createAmountConfig(List.of(
                            new ThresholdConfig(10, BigDecimal.ONE),
                            new ThresholdConfig(12, BigDecimal.TEN),
                            new ThresholdConfig(12, BigDecimal.TWO))));
        }

        @Test
        @DisplayName("Vale above 100 for percentage-based policy configuration should fail")
        public void above100PercentShouldFail() {
            assertThrows(
                    ThresholdException.class,
                    () -> configCtrl.createPercentageConfig((List.of(
                            new ThresholdConfig(10, BigDecimal.ONE),
                            new ThresholdConfig(12, BigDecimal.TEN),
                            new ThresholdConfig(15, BigDecimal.valueOf(120))))));
        }

        @Test
        @DisplayName("Lack of threshold in configuration should fail")
        public void noThresholdShouldFail() {
            assertThrows(
                    ThresholdException.class,
                    () -> configCtrl.createAmountConfig(Collections.emptyList()));
        }
    }

    @Test
    @DisplayName("Three times calculation, with discount configuration modifications meanwhile")
    public void complexScenario() {

        //Part 1: get amount based calculation
        var result = discountCtrl.getPrice(TEST_UUID, 10, "amount");
        checkDiscountResult(result,TEST_UUID, 10, "amount", bd("578.80"), bd("576.80"));

        //Part 2: modify amount based calculation configuration
        var original = configCtrl.readAmountPolicy();
        configCtrl.createAmountConfig(List.of(
                new ThresholdConfig(20, bd("2")),
                new ThresholdConfig(30, bd("3")),
                new ThresholdConfig(10, bd("1"))));

        //Part 3: get different amount based calculation
        result = discountCtrl.getPrice(TEST_UUID, 10, "amount");
        checkDiscountResult(result,TEST_UUID, 10, "amount", bd("578.80"), bd("577.80"));

        //Part 4: restore original configuration
        configCtrl.createAmountConfig(original.thresholds());

        //Part 5: get calculation again - should be exactly the same as at the beginning
        result = discountCtrl.getPrice(TEST_UUID, 10, "amount");
        checkDiscountResult(result,TEST_UUID, 10, "amount", bd("578.80"), bd("576.80"));
    }

    private static void checkDiscountResult(
            CalculationResult result,
            String expectedUuid,
            int expectedAmount,
            String expectedPolicy,
            BigDecimal expectedOriginalPrice,
            BigDecimal expectedFinalPrice) {
        assertThat(result.uuid()).isEqualTo(expectedUuid);
        assertThat(result.amount()).isEqualTo(expectedAmount);
        assertThat(result.discountPolicy()).isEqualTo(expectedPolicy);
        assertThat(result.originalPrice()).isEqualTo(expectedOriginalPrice);
        assertThat(result.finalPrice()).isEqualTo(expectedFinalPrice);
    }

    private static BigDecimal bd(String value) {
        return new BigDecimal(value);
    }
}
