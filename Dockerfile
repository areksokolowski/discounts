FROM eclipse-temurin:21-jdk-alpine
VOLUME /.repo
ARG JAR_FILE
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
