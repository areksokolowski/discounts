# Discount calculator 

## Running the service
### IntelliJ
**Prerequisites:** IntelliJ installed
* import this project as gradle project
* run `DiscountApplication` class
* navigate to [Swagger UI](http://localhost:8080/swagger-ui/index.html])

### Containerized
**Prerequisites:** Java 21, podman or docker   
* build jar with command: `gradlew bootJar`
* build image with command: `docker build --build-arg JAR_FILE=build/libs/\*.jar -t areso/discount .`
* run the image with command: `docker run -p 8080:8080 areso/discount`
* navigate to [Swagger UI](http://localhost:8080/swagger-ui/index.html])

### Testing API
Couple of example API calls are located in `scripts/test-requests.http` file. 

**NOTE 1**: I used `podman` as a containerization solution, but this should be working with `docker` without any changes.

**NOTE 2**: Provided run command does not mount any persistent storage, so persistence is working within container and changes are not preserved between container relaunches. In order to have persistence, provide `.repo` volume mapping in run command, for example: `docker run -p 8080:8080 -v $HOME/.repo:/.repo areso/discount`

## Assumptions
* Product data is provided by some external service. I implemented access to product UUIDs and prices with `PriceService` class. This implementation it uses static resources, but in real-life scenario it would call some external REST service.
* In order to keep the project small, I didn't use any external DB system for persistence, just pure filesystem. Additionally, there is a `no-storage` Spring profile. When active, then file system is not being used for persistence at all.
* Metrics are available with actuator. Custom metric, price calculation counter, is available in application at [this link](http://localhost:8080/actuator/metrics/discount.calculations.count).
* Service does not have any security features. In real-life scenario this aspect needs to be addressed, but in case of this project I decided to keep it simple.
* I added some business logic constraints, like: discount cannot be 0 or less, percentage discount have to be below 100, only one discount related with one threshold 